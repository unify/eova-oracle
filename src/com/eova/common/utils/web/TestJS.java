package com.eova.common.utils.web;

public class TestJS {
	public static void main(String[] args) {
		String s = "console.log(document.cookie)";
		StringBuilder sb = new StringBuilder();
		for (char c : s.toCharArray()) {
			sb.append("&#x" + Integer.toHexString((int) c) + ";");
		}
		System.out.println(sb.toString());
	}
}
