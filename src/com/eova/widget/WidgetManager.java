/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.widget;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.eova.aop.AopContext;
import com.eova.aop.MetaObjectIntercept;
import com.eova.common.utils.xx;
import com.eova.config.EovaConst;
import com.eova.config.PageConst;
import com.eova.core.menu.config.TreeConfig;
import com.eova.core.meta.MetaDataType;
import com.eova.core.object.config.MetaObjectConfig;
import com.eova.core.object.config.TableConfig;
import com.eova.engine.DynamicParse;
import com.eova.engine.EovaExp;
import com.eova.model.Menu;
import com.eova.model.MetaField;
import com.eova.model.MetaObject;
import com.eova.template.common.config.TemplateConfig;
import com.eova.template.common.util.TemplateUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 组件公共业务
 *
 * @author Jieven
 *
 */
public class WidgetManager {

	/**
	 * 构建查询
	 *
	 * @param ctrl
	 * @param object
	 * @param menu
	 * @param intercept
	 * @param parmList
	 * @return
	 * @throws Exception
	 */
	public static String buildQuery(Controller ctrl, MetaObject object, Menu menu, MetaObjectIntercept intercept, List<Object> parmList) throws Exception {
		String sql = "";

		String filter = object.getStr("filter");
		// 菜单初始过滤条件优先级高于对象初始过滤条件
		if (menu != null) {
			String menuFilter = menu.getStr("filter");
			if (!xx.isEmpty(menuFilter))
				filter = menuFilter;
		}
		// if (!xx.isEmpty(filter)) {
		// // 不对超级管理员做数据限制
		// User user = (User) ctrl.getSessionAttr(EovaConst.USER);
		// if (user.getRid() == EovaConst.ADMIN_RID) {
		// filter = null;
		// }
		// }

		// 获取条件
		String where = WidgetManager.getWhere(ctrl, object.getFields(), parmList, filter);

		// 获取排序
		String sort = WidgetManager.getSort(ctrl, object.getStr("default_order"));

		// 分页查询Grid数据
		String view = object.getView();
		sql = "from " + view;

		// 查询前置任务
		if (intercept != null) {
			AopContext ac = new AopContext(ctrl);
			intercept.queryBefore(ac);

			// 自定义查询SQL
			if (!xx.isEmpty(ac.sql)) {
				sql = "from " + String.format("(%s) AS T", ac.sql);
				where = "";
				sort = "";
				parmList.clear();
			} else {
				// 追加条件
				if (!xx.isEmpty(ac.condition)) {
					where += ac.condition;
					if (!xx.isEmpty(ac.params))
						parmList.addAll(ac.params);
				}
				// 覆盖条件
				if (!xx.isEmpty(ac.where)) {
					where = ac.where;
					parmList.clear();
					if (!xx.isEmpty(ac.params))
						parmList.addAll(ac.params);
				}
				// 覆盖排序
				if (!xx.isEmpty(ac.sort)) {
					sort = ac.sort;
				}
			}
		}

		sql += String.format(" %s %s ", where, sort);

		return sql;
	}

	/**
	 * 获取排序
	 *
	 * @param c
	 * @param eo 元对象
	 * @param order 指定排序
	 * @return
	 */
	public static String getSort(Controller c, String order) {

		// 动态解析变量和逻辑运算 TODO 在外面都会解析表达式
		// order = DynamicParse.buildSql(order, c.getSessionAttr(EovaConst.USER));

		String sql = "";

		// 指定默认排序方式
		if (!xx.isEmpty(order)) {
			if (!order.toLowerCase().contains("order by"))
				sql += " order by ";
			sql += order;
		}

		// 当前Request的排序方式
		String orderField = c.getPara(PageConst.SORT, "");// 获取排序字段
		String orderType = c.getPara(PageConst.ORDER, "");// 获取排序方式
		if (!xx.isEmpty(orderField)) {
			sql = " order by " + orderField + ' ' + orderType;
		}

		return sql;
	}

	public static String getSort(Controller c) {
		return getSort(c, null);
	}

	/**
	 * 获取条件
	 *
	 * @param c
	 * @param eo
	 * @param eis
	 * @param parmList SQL参数
	 * @return
	 */
	public static String getWhere(Controller c, List<MetaField> eis, List<Object> parmList, String where) {

		// 动态解析变量和逻辑运算
		where = DynamicParse.buildSql(where, c.getSessionAttr(EovaConst.USER));

		StringBuilder sb = new StringBuilder();

		boolean isWhere = true;
		for (MetaField ei : eis) {
			// sql where 初始化
			if (isWhere) {
				// 存在初始过滤条件
				if (!xx.isEmpty(where)) {
					// 补where
					if (!where.toString().toLowerCase().trim().startsWith("where")) {
						sb.insert(0, " where ");
					} else {
						sb.append(" ");
					}
					sb.append(where + " ");
				} else {
					sb.append(" where 1=1 ");
				}
				isWhere = false;
			}
			
			// 跳过虚拟字段
			if (ei.isVirtual()) {
				continue;
			}

			String key = ei.getEn();
			// 给查询表单添加前缀，防止和系统级别字段重名
			String value = c.getPara(PageConst.QUERY + key, "").trim();
			String start = c.getPara(PageConst.START + key, "").trim();
			String end = c.getPara(PageConst.END + key, "").trim();
			
			// 当前字段 既无文本值 也无范围值，说明没填，直接跳过
			if ((xx.isEmpty(value) || value.equals("-1")) && xx.isAllEmpty(start, end)) {
				continue;
			}
			// 范围值只填一个，默认两个值相同
			if (xx.isEmpty(start))
				start = end;
			if (xx.isEmpty(end))
				end = start;

			// 布尔框需要转换值
			value = TemplateUtil.convertValue(ei, value).toString();

			String type = ei.getStr("type");
			if (ei.getDataTypeName().toLowerCase().contains("int") && !xx.isEmpty(value)) {
				// 数字类型都是精确查询
				sb.append(" and " + key + " = ?");
				parmList.add(value);
			} else if (type.equals(MetaField.TYPE_TEXT) || type.equals(MetaField.TYPE_TEXTS) || type.equals(MetaField.TYPE_EDIT)) {
				// 文本类都是模糊条件
				sb.append(" and " + key + " like ?");
				parmList.add("%" + value + "%");
			} else if (type.equals(MetaField.TYPE_NUM)) {
				// 数值类的都是精准查询
				String condition = c.getPara(PageConst.COND + key, "").trim();

				if (condition.equals("=")) {
					sb.append(" and ? = " + key);
					parmList.add(start);
				} else if (condition.equals("<")) {
					sb.append(" and ? > " + key);
					parmList.add(start);
				} else if (condition.equals(">")) {
					sb.append(" and ? < " + key);
					parmList.add(start);
				} else {
					// 只选一个值无效
					if (xx.isOneEmpty(start, end)) {
						continue;
					}

					if (condition.equals("∩")) {
						sb.append(" and ? < " + key + " and " + key + " < ?");
						parmList.add(start);
						parmList.add(end);
					} else if (condition.equals("U")) {
						sb.append(" and ? > " + key + " or " + key + " < ?");
						parmList.add(start);
						parmList.add(end);
					}
				}

				// 时间类的都是范围查询
			} else if (type.equals(MetaField.TYPE_TIME) || type.equals(MetaField.TYPE_DATE)) {
				// 只选一个时间无效
				if (xx.isOneEmpty(start, end)) {
					continue;
				}

				if (xx.isOracle()) {
					sb.append(" and " + key + " >= to_date(?,'yyyy-mm-dd') and " + key + " < to_date(?,'yyyy-mm-dd')+1");
				} else {
					sb.append(" and date(" + key + ") >= ? and date(" + key + ") <= ?");
					// sb.append(" and " + key + " between ? and ?");时间范围查询
				}

				parmList.add(start);
				parmList.add(end);
			} else {
				if (ei.getBoolean("is_multiple")) {
					// 多值条件
					sb.append(" and (");
					for (String val : value.split(",")) {
						if (!sb.toString().endsWith(" (")) {
							sb.append(" or ");
						}
						if (xx.isMysql()) {
							sb.append(String.format("FIND_IN_SET(?, %s)", key));
							parmList.add(val);
						} else if (xx.isOracle()) {
							// 意思 在 ,1,2,21,22,30, 查找 ,2,
							sb.append(String.format("instr(','||%s||',' ,  ',%s,') > 0", key, val));
						} else {
							sb.append(key + " like ").append("?");
							parmList.add('%' + val + '%');
						}
					}
					sb.append(")");
				} else {
					// 单值条件
					sb.append(" and " + key + " = ?");
					parmList.add(value);
				}
			}

			// 保持条件值回显
			ei.put("value", value);
		}

		return sb.toString();
	}

	/**
	 * 构建翻译查询条件
	 *
	 * @param sql
	 */
	public static void buildWhere(StringBuilder sql) {
		// 没有where关键字(说明表达式没有带条件)，自动添加where头，方便后续跟 and id in(x,x,x)
		if (!sql.toString().toLowerCase().contains("where")) {
			sql.append(" where 1=1 ");
		}
		// 自动切断后续条件，翻译是根据当前页所有ID查询外表，不需要关心额外条件
		// eg. where id = ${user.id}
	}

	/**
	 * 添加where条件
	 *
	 * @param se
	 * @param condition
	 * @return
	 */
	public static String addWhere(EovaExp se, String condition) {

		String select = se.simpleSelect;
		String from = se.from;
		String where = se.where;

		if (xx.isEmpty(where)) {
			where = " where " + condition;
		} else {
			where += " and " + condition;
		}

		return select + from + where;
	}

	public static void convertValueByExp(Controller c, List<MetaField> eis, List<Record> reList, String... excludeFields) {
		// 根据表达式翻译显示CN(获取当前字段所有的 查询结果值，拼接成 字符串 用于 结合表达式进行 in()查询获得cn 然后替换之)
		F1: for (MetaField ei : eis) {
			// 获取存在表达式的列名
			String en = ei.getEn();
			// 排除不需要翻译的字段
			if (!xx.isEmpty(excludeFields)) {
				for (String field : excludeFields) {
					if (field.equals(en)) {
						continue F1;
					}
				}
			}
			// 只翻译需要显示的字段
			if (!ei.getBoolean("is_show")) {
				continue;
			}
			// 获取控件表达式
			String exp = ei.getStr("exp");
			if (xx.isEmpty(exp)) {
				continue;
			}
			// System.out.println(en + " EovaExp:" + exp);
			// in 条件值
			Set<String> ids = new HashSet<String>();
			if (!xx.isEmpty(reList)) {
				for (Record re : reList) {
					String value = re.get(en, "").toString();
					if (value.contains(",")) {
						// 多值
						for (String val : value.split(",")) {
							ids.add(val);
						}
					} else {
						// 单值
						ids.add(value);
					}
				}
			}

			exp = DynamicParse.buildSql(exp, c.getSessionAttr(EovaConst.USER));

 			EovaExp se = new EovaExp(exp);
			String select = se.simpleSelect;
			String where = se.where;
			String from = se.from;
			String pk = se.pk;
			String cn = se.cn;

			// 清除value列查询条件，防止干扰翻译SQL条件
			where = filterValueCondition(where, pk);
			// PS:底部main有测试用例

			StringBuilder sql = new StringBuilder();
			sql.append(select);
			sql.append(from);
			sql.append(where);
			// 构建特殊翻译查询条件
			buildWhere(sql);

			// 查询本次所有翻译值
			if (!xx.isEmpty(ids)) {
				sql.append(" and ").append(pk);
				sql.append(" in(");
				// 根据当前页数据value列查询外表name列
				for (String id : ids) {
					// TODO There might be a SQL injection risk warning
					sql.append(xx.format(id)).append(",");
				}
				sql.deleteCharAt(sql.length() - 1);
				sql.append(")");
			}

			List<Record> translates = Db.use(se.ds).find(sql.toString());

			// 翻译匹配项
			for (Record re : reList) {
				Object o = re.get(en);
				// 空字段无法翻译
				if (o == null) {
					re.set(en, "");
					continue;
				}

				String value = o.toString();

				String text = "";
				if (value.contains(",")) {
					// 多值
					for (String val : value.split(",")) {
						text += translateValue(pk, cn, translates, val);
						text += ',';
					}
					text = xx.delEnd(text, ",");
				} else {
					text = translateValue(pk, cn, translates, value);
				}
				re.set(en, text);
			}
		}
	}

	/**
	 * 将value翻译text
	 *
	 * @param pk value
	 * @param cn text
	 * @param translates 字典集合
	 * @param re 待翻译对象
	 * @param value
	 */
	public static String translateValue(String pk, String cn, List<Record> translates, String value) {
		for (Record r : translates) {
			// 翻译前的值(默认为第1列查询值)
			String key = r.get(pk).toString();
			// 翻译后的值(默认为第2列查询值)
			String name = r.get(cn).toString();
			if (value.equals(key)) {
				return name;
			}
		}
		return value;
	}

	/**
	 * 通过Form构建数据
	 *
	 * @param c 控制器
	 * @param eis 对象属性
	 * @param record 当前操作对象数据集
	 * @param pkName 主键字段名
	 * @return 视图表数据分组
	 */
	public static Map<String, Record> buildData(Controller c, MetaObject object, Record record, String pkName, boolean isInsert) {
		Map<String, Record> datas = new HashMap<String, Record>();

		// 获取字段当前的值
		for (MetaField item : object.getFields()) {
			// System.out.println(item.getEn() +'='+ c.getPara(item.getEn()));
			String type = item.getStr("type");// 控件类型
			String key = item.getEn();// 字段名

			// 获当前字段 Requset Parameter Value，禁用=null,不填=""
			Object value = c.getPara(key);
			// 值的预处理
			value = TemplateUtil.convertValue(item, value);
			// 自动类型转换
			value = MetaDataType.convert(item, value);
			// System.out.println(String.format("FormData：%s=%s", key, value));
			// 新增跳过自增长字段(新增时隐藏)
			if (value == null && type.equals(MetaField.TYPE_AUTO)) {
				continue;
			}

			// 当前字段为空(禁用/隐藏/非字符并未填写 且没有默认值),说明不需要持久化，直接跳过
			if (value == null) {
				continue;
			}

			// 全量的数据
			record.set(key, value);
		}
		return datas;
	}

	/**
	 * 视图持久化操作(暂时废弃)
	 *
	 * @param mode 操作模式 add/update
	 * @param object 元对象
	 * @param data 视图当前操作所有数据
	 */
	@Deprecated
	@SuppressWarnings("rawtypes")
	public static void operateView(String mode, MetaObject object, Record data) {
		MetaObjectConfig config = object.getConfig();
		LinkedHashMap<String, TableConfig> view = config.getView();
		Iterator iter = view.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			String tableName = (String) entry.getKey();
			TableConfig tc = (TableConfig) entry.getValue();

			// 当前表的条件字段
			String whereField = tc.getWhereField();
			// 当前表的条件字段对应字段
			String paramField = tc.getParamField();

			// 按表获取持久化对象
			// Record po = recordGroup.get(table);
			Record po = getRecordByTableName(object, data, tableName);
			if (po == null) {
				continue;
			}

			// 主键可能并不是显示字段，所以需要手工获取关联字段的值
			if (!po.getColumns().containsKey(whereField)) {
				po.set(whereField, data.get(paramField));
			}

			if (mode.equals(TemplateConfig.UPDATE)) {
				// 按本表条件的关联字段进行更新
				Db.use(object.getDs()).update(tableName, whereField, po);
			} else if (mode.equals(TemplateConfig.ADD)) {
				// 按本表条件的关联字段进行更新
				Db.use(object.getDs()).save(tableName, po);
			}
		}
	}

	/**
	 * 根据表名从当前操作数据中构建该表所属字段数据
	 *
	 * @param object 元数据
	 * @param data 当前操作数据集
	 * @param tableName 当前持久化表名
	 * @return 当前持久化数据对象
	 */
	private static Record getRecordByTableName(MetaObject object, Record data, String tableName) {
		List<MetaField> fields = object.getFields();
		Record r = null;
		for (MetaField f : fields) {
			if (tableName.equals(f.getStr("table_name"))) {
				String en = f.getEn();
				Object val = data.get(en);
				if (val == null)
					continue;
				if (r == null)
					r = new Record();
				r.set(en, val);
			}
		}
		return r;
	}

	/**
	 * 过滤指定查询条件
	 *
	 * @param where 查询条件Sql
	 * @param colName 要过滤的列名
	 * @return 过滤后的Sql
	 */
	public static String filterValueCondition(String where, String colName) {
		if (where.contains(colName)) {
			where = where.replaceAll("( " + colName + ".*?)and", "");
		}
		return where;
	}

	/**
	 * 获取关联参数
	 *
	 * @param c
	 * @return
	 */
	public static Record getRef(Controller c) {
		Record r = new Record();

		try {
			String ref = c.getPara("ref");
			if (xx.isEmpty(ref)) {
				return r;
			}
			String[] fields = ref.split(",");
			for (String field : fields) {
				String[] strs = field.split(":");
				r.set(strs[0], strs[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return r;
		}

		return r;
	}

	/**
	 * 构建关联参数值
	 *
	 * @param object
	 */
	public static void buildRef(Controller ctrl, MetaObject object) {
		for (MetaField ei : object.getFields()) {
			String key = ei.getEn();

			Record ref = getRef(ctrl);
			if (ref != null && !xx.isEmpty(ref.get(key))) {
				ei.put("value", ref.get(key));
				ei.put("is_disable", true);
			}
		}
	}

	public static void main(String[] args) {
		/*
		 * 清除value列查询条件，防止干扰翻译SQL条件 自定义SQL eg. where uid = ${user.id} and type = 3 自动翻译SQL eg. and uid in(1,2,3)
		 */
		// String where = "where uid = ${user.id} and type = 1 and state = 1";
		// String where = "where type = 2 and uid = ${user.id} and state = 2";
		String where = "where type = 3 and uid = ${user.id} and state = 3 and uid = 3 and uid in (1,2,3)";
		String pk = "uid";
		System.out.println(where);
		where = filterValueCondition(where, pk);
		System.out.println(where);

		// addwhere
		System.out.println(addWhere(new EovaExp("select * from users where 1=1"), "id = 1"));
		System.out.println(addWhere(new EovaExp("select id ID,name CN from users where a = 1"), "id = 1"));
	}

	/**
	 * 向上递归查找父节点数据
	 * 
	 * @param treeConfig 树配置
	 * @param view 数据源表名
	 * @param list 显示结果数据
	 * @param parents 待查找数据
	 */
	public static void findParent(TreeConfig treeConfig, String ds, String select, String view, List<Record> list, List<Record> parents) {
		Set<String> pids = new HashSet<>();
		for (Record x : parents) {
			pids.add(x.get(treeConfig.getParentField()).toString());
		}
		if (!xx.isEmpty(pids)) {
			if (pids.size() == 1 && pids.toArray()[0].equals(treeConfig.getRootPid())) {
				// 到根节点停止递归
				return;
			}
			parents = Db.use(ds).find(String.format("%s from %s where %s in (%s)", select, view, treeConfig.getIdField(), xx.join(pids, "'", ",")));

			// 父节点去重
			for (Record p : parents) {
				boolean b = false;
				for (Record x : list) {
					if (x.get(treeConfig.getIdField()).toString().equals(p.get(treeConfig.getIdField()).toString())) {
						b = true;
						break;
					}
				}
				if (!b)
					list.add(p);
			}
			findParent(treeConfig, ds, select, view, list, parents);
		}
	}
}